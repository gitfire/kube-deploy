FROM node:10-alpine 

WORKDIR /app
COPY Gruntfile.js *.json ./
RUN npm install && npm run grunt
COPY . ./
USER node
CMD [ "npm", "start" ]
